﻿using Newtonsoft.Json;
using System.IO;

namespace UnityProject.Serialization
{
    public class JsonDefaultSerializer : ISerializer
    {
        private JsonSerializerSettings settings;
        private JsonSerializer serializer;

        public JsonDefaultSerializer()
        {
            var _contractResolver = new JsonContractResolver();
            this.settings = new JsonSerializerSettings() { ContractResolver = _contractResolver };
            this.settings.Formatting = Formatting.Indented;
            this.settings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            this.serializer = JsonSerializer.Create(settings);
        }

        public string Serialize<T>(T resource)
        {
            using (StringWriter writer = new StringWriter())
            {
                serializer.Serialize(writer, resource, typeof(T));
                return writer.ToString();
            }
        }

        public T Deserialize<T>(string resource)
        {
            using (StringReader reader = new StringReader(resource))
            {
                T result = (T)serializer.Deserialize(reader, typeof(T));
                return result;
            }
        }
    }
}
