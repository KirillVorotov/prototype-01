﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Reflection;
using UnityEngine;

namespace UnityProject.Serialization
{
    public class JsonContractResolver : DefaultContractResolver
    {
        protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
        {
            var prop = base.CreateProperty(member, memberSerialization);

            if (prop != null)
            {
                if(prop.DeclaringType == typeof(Vector3) && prop.PropertyName == "normalized")
                {
                    prop.ShouldSerialize = instance => { return false; };
                }

                if (prop.DeclaringType == typeof(Vector3) && prop.PropertyName == "magnitude")
                {
                    prop.ShouldSerialize = instance => { return false; };
                }

                if (prop.DeclaringType == typeof(Vector3) && prop.PropertyName == "sqrMagnitude")
                {
                    prop.ShouldSerialize = instance => { return false; };
                }

                if (!prop.Writable)
                {
                    var property = member as PropertyInfo;
                    if (property != null)
                    {
                        var hasPrivateSetter = property.GetSetMethod(true) != null;
                        prop.Writable = hasPrivateSetter;
                    }
                }
            }
            return prop;
        }
    }
}
