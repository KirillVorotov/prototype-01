﻿namespace UnityProject.Serialization
{
    public interface ISerializer
    {
        string Serialize<T>(T resource);
        T Deserialize<T>(string resource);
    }
}
