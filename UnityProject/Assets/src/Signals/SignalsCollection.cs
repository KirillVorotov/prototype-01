﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace UnityProject.Signals
{
    public class SignalsCollection<TSignalInterface> where TSignalInterface : class
    {
        protected readonly Dictionary<Type, TSignalInterface> signals = new Dictionary<Type, TSignalInterface>();

        protected SignalsCollection(List<TSignalInterface> signals)
        {
            foreach (var signal in signals)
            {
                this.signals.Add(signal.GetType(), signal);
            }
        }

        public T GetSignal<T>() where T : class
        {
            TSignalInterface result;
            if (!signals.TryGetValue(typeof(T), out result))
                Debug.LogErrorFormat("No such signal in collection: {0}", typeof(T));
            return result as T;
        }
    }
}
