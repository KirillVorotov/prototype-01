﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Zenject;

namespace UnityProject.Signals
{
    public interface ICameraSignal { }

    public class MoveCameraSignal : Signal<MoveCameraSignal, Vector3>, ICameraSignal { }
    public class RotateCameraSignal : Signal<RotateCameraSignal, Vector2>, ICameraSignal { }
    public class ChangeDistanceCameraSignal : Signal<ChangeDistanceCameraSignal, float>, ICameraSignal { }
}
