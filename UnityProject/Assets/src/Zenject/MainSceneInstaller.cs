﻿using UnityEngine;
using UnityProject.Signals;
using Zenject;

namespace UnityProject.Zenject
{
    public class MainSceneInstaller : MonoInstaller
    {
        [SerializeField]
        private GameObjectContext cameraContext;

        public override void InstallBindings()
        {
            Container.BindAllInterfacesAndSelf<SignalsCollection<ICameraSignal>>().To<SignalsCollection<ICameraSignal>>().AsSingle().NonLazy();
            Container.BindFactory<TopDownCameraSettings, TopDownCameraFacade, TopDownCameraFacade.Factory>().FromSubContainerResolve().ByPrefab<TopDownCameraInstaller>(cameraContext);

            Container.BindAllInterfacesAndSelf<TopDownCameraSettings>().To<TopDownCameraSettings>().FromInstance(LoadCameraSettings(System.IO.Path.Combine(Application.dataPath, "Config/TopDownCameraSettings.cfg"))).AsSingle();

            #region Camera Signals

            Container.BindAllInterfaces<MoveCameraSignal>().To<MoveCameraSignal>();
            Container.BindAllInterfaces<RotateCameraSignal>().To<RotateCameraSignal>();
            Container.BindAllInterfaces<ChangeDistanceCameraSignal>().To<ChangeDistanceCameraSignal>();

            #endregion
        }

        private TopDownCameraSettings LoadCameraSettings(string path)
        {
            TopDownCameraSettings settings;

            using (var sr = new System.IO.StreamReader(path))
            {
                var serializer = new Serialization.JsonDefaultSerializer();
                var s = sr.ReadToEnd();
                settings = serializer.Deserialize<TopDownCameraSettings>(s);
            }
            return settings;
        }
    }
}