﻿using UnityEngine;
using UnityProject.Serialization;
using Zenject;

namespace UnityProject.Zenject
{
    public class GlobalInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.BindAllInterfaces<JsonDefaultSerializer>().To<JsonDefaultSerializer>().AsSingle().NonLazy();
        }
    }
}
