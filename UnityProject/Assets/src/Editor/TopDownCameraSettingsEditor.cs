﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;
using UnityProject.Serialization;

namespace UnityProject.Editor
{
    public class TopDownCameraSettingsEditor
    {
        [MenuItem("Assets/Create/Top Down Camera Settings")]
        public static void CreateTopDownCameraSettings()
        {
            var settings = new TopDownCameraSettings()
            {
                MinDistance = 10f,
                MaxDistance = 50f,
                StartDistance = 20f,

                FieldOfView = 60f,
                Orthographic = false,
                OrthographicSize = 10f,
                NearClipPlane = 0.3f,
                FarClipPlane = 1000f,
            };
            var serializer = new JsonDefaultSerializer();
            
            using (var sw = new StreamWriter(Path.Combine(Application.dataPath, "Config/TopDownCameraSettings.cfg")))
            {
                var s = serializer.Serialize(settings);
                sw.Write(s);
            }
        }
    }
}
