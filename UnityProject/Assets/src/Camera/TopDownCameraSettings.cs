﻿using System;
using UnityEngine;

namespace UnityProject
{
    [Serializable]
    public class TopDownCameraSettings
    {
        public float MinDistance;
        public float MaxDistance;
        public float StartDistance;

        public float FieldOfView;
        public bool Orthographic;
        public float OrthographicSize;
        public float NearClipPlane;
        public float FarClipPlane;
    }
}
