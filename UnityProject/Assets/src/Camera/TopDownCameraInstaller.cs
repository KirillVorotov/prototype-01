﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Zenject;

namespace UnityProject
{
    public class TopDownCameraInstaller : MonoInstaller
    {
        [SerializeField, Inject(Optional = true)]
        private TopDownCameraSettings settings;

        public override void InstallBindings()
        {
#if UNITY_EDITOR
            if (settings == null)
                settings = new TopDownCameraSettings();
#endif
            Container.BindInstance(settings);
        }
    }
}
