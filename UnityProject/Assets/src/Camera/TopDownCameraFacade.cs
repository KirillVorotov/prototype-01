﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityProject.Signals;
using Zenject;

namespace UnityProject
{
    public class TopDownCameraFacade : MonoBehaviour, ITickable
    {
        protected SignalsCollection<ICameraSignal> cameraSignals;
        protected TopDownCameraBehaviour topDownCamera;
        protected new Camera camera;
        protected TopDownCameraSettings settings;

        [Inject]
        private void Initialize(TopDownCameraBehaviour topDownCamera, Camera camera, SignalsCollection<ICameraSignal> cameraSignals, TopDownCameraSettings settings)
        {
            this.topDownCamera = topDownCamera;
            this.camera = camera;
            this.cameraSignals = cameraSignals;
            this.settings = settings;

            topDownCamera.Initialize();
            InitializeCamera(settings);

            cameraSignals.GetSignal<MoveCameraSignal>().Listen(OnMoveCamera);
            cameraSignals.GetSignal<RotateCameraSignal>().Listen(OnRotateCamera);
            cameraSignals.GetSignal<ChangeDistanceCameraSignal>().Listen(OnChangeDistance);
        }

        public void Tick()
        {
            topDownCamera.Tick(Time.deltaTime);
        }

        private void InitializeCamera(TopDownCameraSettings settongs)
        {
            this.camera.fieldOfView = settings.FieldOfView;
            this.camera.orthographic = settings.Orthographic;
            this.camera.orthographicSize = settings.OrthographicSize;
            this.camera.nearClipPlane = settings.NearClipPlane;
            this.camera.farClipPlane = settings.FarClipPlane;
        }

        private void OnMoveCamera(Vector3 direction)
        {
            throw new NotImplementedException();
        }

        private void OnRotateCamera(Vector2 direction)
        {
            throw new NotImplementedException();
        }

        private void OnChangeDistance(float distance)
        {
            throw new NotImplementedException();
        }

        private void OnDestroy()
        {
            cameraSignals.GetSignal<MoveCameraSignal>().Unlisten(OnMoveCamera);
            cameraSignals.GetSignal<RotateCameraSignal>().Unlisten(OnRotateCamera);
            cameraSignals.GetSignal<ChangeDistanceCameraSignal>().Unlisten(OnChangeDistance);
            Destroy(topDownCamera.gameObject);
            Destroy(camera.gameObject);
        }

        public class Factory : Factory<TopDownCameraSettings, TopDownCameraFacade> { }
    }
}
